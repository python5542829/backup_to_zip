# !/usr/bin/env python3
"""
Backup to Zip

Version: 1.0
Author: Eugenio Gonzalez
Description: Program that backs up a file or directory to a zip file.
Parameters: the script takes the file/directory to be backed up and the destination path (optional).
    the script can also take only the file/directory to be backed up, the destination will be the working directory.
"""

import os
import sys
import zipfile
from pathlib import Path
from datetime import datetime
import traceback


def get_current_date() -> str:
    """
    Returns the current date in the following format: dd/mm/yyyyy

    """
    current_datetime = datetime.now()
    return current_datetime.strftime("%d-%m-%Y")


def zip_single_file(zip_file: zipfile, source_filename: str) -> None:
    """
    Backs up a single file to a zip file.

    :param zip_file: ZipFile object
    :param source_filename: absolute path of the file to be backed up
    """
    zip_file.write(source_filename, arcname=os.path.basename(source_filename), compress_type=zipfile.ZIP_DEFLATED)


def backup_directory(zip_file: zipfile, source_filename: str) -> None:
    """
    Backup a directory in a zip file

    :param zip_file: ZipFile object
    :param source_filename: absolute path of the directory to be backed up
    """
    base_path = ""
    for root, directories, files in os.walk(source_filename):
        base_path += os.path.basename(root) + "/"
        print(f"Compressing the files in the folder {root}")
        for filename in files:
            # join the two strings to form the full filepath.
            filepath = os.path.join(root, filename)
            zip_file.write(filepath, base_path + '/' + filename, compress_type=zipfile.ZIP_DEFLATED)


def run_program(source_path: str, destination_path: str) -> str:
    """
    This script takes in two file paths, `source_path` and `destination_path`,
    as input and performs several operations on them:
        - Convert the relative paths to absolute paths
        - Check if the files or directories at the given paths exist or not, and raises an error if they don't.
        - Zip the file or directory at the `source_path` and save it in the `destination_path` directory
        - return the message of the location where the file is saved
    :param source_path: absolute path of the file to be backed up
    :param destination_path: absolute path of the directory in which the zip file is to be saved
    :return: absolute path of the zip file if no error occurred
    :raise ValueError: If the source/destination path does not exist
    """
    # Ensure that the paths are absolute paths
    if not os.path.isabs(source_path):
        source_path = os.path.abspath(source_path)
    if not os.path.isabs(destination_path):
        destination_path = os.path.abspath(destination_path) + '/'

    # Check that the paths exist
    if not os.path.exists(source_path):
        # print(f"Error: file {source_path} does not exist")
        # sys.exit()
        raise ValueError(f"Error: file {source_path} does not exist")
    if not os.path.exists(destination_path):
        # print(f"Error: file {source_path} does not exist")
        # sys.exit()
        raise ValueError(f"Error: file {destination_path} does not exist")

    # zip file
    zip_path = destination_path \
        + os.path.basename(Path(source_path).stem) \
        + '_' \
        + get_current_date() \
        + '.zip'
    with zipfile.ZipFile(zip_path, 'w') as backup_zip:
        # Check whether it is a single file or a directory
        if os.path.isfile(source_path):
            zip_single_file(backup_zip, source_path)
        else:
            backup_directory(backup_zip, source_path)

    return f"file saved in {destination_path}"


if __name__ == "__main__":
    try:
        # Check for the correct number of command line arguments
        if len(sys.argv) > 3 or len(sys.argv) < 2:
            raise ValueError(f"""
            Please provide at least 2 filenames as command line arguments.
            if one is provided, the working directory will be taken as destination.
            
            Example: python3 {sys.argv[0]} <source_filename> [destination_path]
            """)

        # Get the two filenames from the command line
        source_path = sys.argv[1]
        if len(sys.argv) == 3:
            destination_path = sys.argv[2]
        else:
            destination_path = os.getcwd() + '/'

        # Run the program
        print(run_program(source_path, destination_path))
    except ValueError as e:
        print(e)
        with open('errorInfo.txt', 'a') as errorFile:
            message = f"===> {datetime.now()} <===\n" \
                      f"{traceback.format_exc()}\n"
            errorFile.write(message)
            print('The traceback info was written to errorInfo.txt.')
