<!-- TOC -->
* [Backup to zip](#backup-to-zip)
  * [Basic usage](#basic-usage)
  * [Linux installation](#linux-installation)
    * [Quick access (create an alias)](#quick-access--create-an-alias-)
<!-- TOC -->
# Backup to zip
Python script that backs up a file/folder to a zip file.

## Basic usage
```sh
python3 backup_to_zip.py <source_filename> [destination_path]
```
## Linux installation
```sh
git clone https://github.com/eugenio-gonzalez00
```

### Quick access (create an alias)
- bash
  ```sh
  echo "alias pzip='python3 ~/backup_to_zip/backup_to_zip.py'" >> ~/.bashrc
  ```
  Now you can run the program like this: `pzip <source_filename> [destination_path]`.
- zsh
  ```sh
  echo "alias pzip='python3 ~/backup_to_zip/backup_to_zip.py'" >> ~/.zshrc
  ```
  Now you can run the program like this: `pzip <source_filename> [destination_path]`.